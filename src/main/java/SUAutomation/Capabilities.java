package SUAutomation;
import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.DesiredCapabilities;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.remote.MobilePlatform;

public class Capabilities
{
	public static   AndroidDriver<AndroidElement> Capabilities() throws MalformedURLException
	{
	 //File appDir = new File("src");
     // File app = new File(appDir, "STHAT.apk");
     DesiredCapabilities cap= new DesiredCapabilities();
     //Capabilities.setCapability(MobileCapabilityType.DEVICE_NAME,"Nexus");
     cap.setCapability(MobileCapabilityType.PLATFORM_NAME,MobilePlatform.ANDROID);
     cap.setCapability(MobileCapabilityType.DEVICE_NAME, "420057c3dc1293f1");
     cap.setCapability("automationName", "uiautomator2");
     cap.setCapability("noReset", true);
     cap.setCapability("appPackage", "com.tmmmt.sthat.beta");
     cap.setCapability("appActivity", "com.tmmmt.sthat.activity.SplashActivity");
     //Capabilities.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, "100");
     //Capabilities.setCapability(MobileCapabilityType.APP, app.getAbsolutePath());
     AndroidDriver<AndroidElement> driver = new AndroidDriver(new URL("http://127.0.0.1:4723/wd/hub"),cap);
	 return driver;
	}
}