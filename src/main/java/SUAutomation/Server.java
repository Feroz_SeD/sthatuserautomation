package SUAutomation;
import org.apache.log4j.Logger;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.touch.TouchActions;
import org.openqa.selenium.internal.FindsById;

import Utility.PageHelper;
import Utility.UserInterface;
import io.appium.java_client.FindsByAndroidUIAutomator;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;

public  class Server extends Main{
	protected AndroidDriver<AndroidElement> driver;
    public Server(AndroidDriver<AndroidElement> driver) {
	this.driver = driver;
	}
	Logger log = Logger.getLogger(Server.class);	
	
	public void StartScreen() throws InterruptedException {
		UserInterface ui = new UserInterface(driver);
		ui.Sleep(3);
		try {
			ui.SthatUrl.click();
			ui.Sleep(6);
			driver.navigate().back();
			ui.DownloadSthatPartner.click();
			ui.Sleep(3);
			driver.navigate().back();
			ui.TermsAndConditions.click();
			ui.Sleep(6);
			driver.navigate().back();
			ui.START.click();
			ui.Sleep(2);
			driver.navigate().back();
			ui.Sleep(1);
			driver.navigate().back();
			log.info("StartScreen UI Testing Successful");
			
		} catch (Exception e) {
			log.error("StartScreen UI Testing Failed");
		}
		
	}
    public void Signup(int x,String MobileNo) throws Exception {
    	 UserInterface ui = new UserInterface(driver);
       	 ui.Sleep(3);
    	 ui.START.click();
    	 ui.Sleep(5);
    	 ui.SpCountryCode.click();
    	 ui.CountryCode.get(x).click();
    	 ui.Sleep(4);
	     ui.MobileNumber.sendKeys(MobileNo);
	     driver.hideKeyboard();
	     ui.SEND.click();
	     ui.Sleep(8);
	     try {
	 			PageHelper Helper = new PageHelper();
	 			String OTP = Helper.getOTP(MobileNo);
	 		    ui.EnterOtp.sendKeys(OTP);	
	 		   ui.Sleep(5);
	 		} 
	 		catch(Exception e) {
	 			log.error("OTP Failed");
	 		}
	     ui.NewAccOK.click();
	     ui.EnterName.sendKeys("Test");
	     ui.EnterEmail.sendKeys("test8@gmail.com");
	     ui.SIGNUP.click();
	     log.info("Signup Successful");
	     
    }
    public void Login(int x,String MobileNo) throws Exception {
    	 UserInterface ui = new UserInterface(driver);
   	     ui.Sleep(3);
   	     ui.START.click();
	     ui.Sleep(5);
	     ui.SpCountryCode.click();
	     ui.CountryCode.get(x).click();
	     ui.Sleep(4);
         ui.MobileNumber.sendKeys(MobileNo);
         driver.hideKeyboard();
         ui.SEND.click();
         ui.Sleep(8);
 		try {
 			PageHelper Helper = new PageHelper();
 			String OTP = Helper.getOTP(MobileNo);
 		    ui.EnterOtp.sendKeys(OTP);	
 		   ui.Sleep(5);
 		} 
 		catch(Exception e) {
 			log.error("OTP Failed");
 		}
         log.info("Login Successful");
    }
    
    public void Logout() throws Exception {
    	UserInterface ui = new UserInterface(driver);
    	ui.Sleep(3);
    	ui.MENU.click();
    	ui.Sleep(2);
    	ui.Logout.click();
    	ui.Sleep(1);
    	ui.logoutYes.click();
    	log.info("Logout Successful");
    }  
    
    public void PostOrder() throws InterruptedException {
    	UserInterface ui = new UserInterface(driver);
    	ui.Sleep(2);
    	ui.TO.click();
    	ui.Sleep(2);
    	ui.SearchListToLoc.get(1).click();
    	ui.Sleep(4);
    	ui.PlaceRequest.click();
    	ui.Sleep(2);
    	log.info("PostOrder Successful");    
	}
    
   public void CancelOrder() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
    	ui.Sleep(5);
    	try {
    		ui.CanelRequest.click();
    		ui.Sleep(2);
        	ui.CancelPaymentYes.click();
        	log.info("CancelOrder Successful");
			
		} catch (Exception e) {
			log.error("No trucks found nearby");
		}    	
    }
   
   public void MenuTesting() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
       ui.Sleep(3);
       try {
    	   BookingHistory();
           Wallet();
           Settings();
           Share();
           CustomerSupport();
          // CustomerSupportNumber(driver);
           log.info("Menu Testing Successful");
		
	} catch (Exception e) {
		 log.error("Menu Testing Failed");
	}
              
	}
   public void BookingHistory() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.BookingHistory.click();
	       ui.BHCancelled.click();
	       ui.BHCompleted.click();
	       ui.BackButton.get(0).click();
	       ui.MENU.click();
	       log.info("BookingHistory Test Successful");
		
	} catch (Exception e) {
		log.error("BookingHistory Test Failed");
	}
	   
	   
   }
  public void Wallet() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.WalletMenu.click();
		   ui.Sleep(2);
	       ui.BanksUrl.click();
	       ui.Sleep(5);
	       driver.navigate().back();
	       ui.Amount.click();
	       ui.Amount.sendKeys("1000");
	       ui.AddMoney.click();
	       ui.Sleep(6);
	       driver.navigate().back();
	       ui.CancelPaymentNo.click();
	       ui.Sleep(2);
	       driver.navigate().back();
	       ui.CancelPaymentYes.click();
	       ui.Sleep(2);
	       ui.Refresh.click();
	       ui.BackButton.get(0).click(); 
	       ui.MENU.click();
	       log.info("Wallet Test Successful");
		
	} catch (Exception e) {
		log.error("Wallet Test Failed");
	}
	  
   }
  public void Settings() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.Settings.click();
		   ui.Sleep(2);
		   ui.SettingsEdit.click();
		   ui.Sleep(1);
		   ui.ProfileCancel.click();
		   ui.SettingsEdit.click();
		   ui.Sleep(5);
		   ui.ProfileUpdate.click();
		   ui.Sleep(1);
		   ui.SmsNotification.click();
		   ui.EmailNotification.click();
		   ui.SmsNotification.click();
		   ui.EmailNotification.click();
		   ui.Rating.click();
		   ui.Sleep(4);
		   driver.navigate().back();
		   ui.LanguageArabic.click();
		   ui.languageDecline.click();
		   ui.LanguageArabic.click();
		   ui.languageApply.click();
		   ui.Sleep(10);
		   ui.MENU.click();
		   ui.Settings.click();
		   ui.LanguageEnglish.click();
		   ui.languageApply.click();
		   ui.Sleep(7);
		   log.info("Settings Test Successful");
		
	} catch (Exception e) {
		log.error("Settings Test Failed");
	}
	  
  }
 public void Share() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.Share.click();
		   ui.Sleep(2);
		   driver.navigate().back();
		   ui.MENU.click();
		   log.info("Share Test Successful");
		
	} catch (Exception e) {
		log.error("Share Test Failed");
	}
	   
 }
 public void CustomerSupport() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.CustomerSupport.click();
		   ui.Sleep(4);
		   ui.CustomerSupportMsg.click();
		   ui.CustomerSupportMsg.sendKeys("Hi, This is Test Message");
		   ui.Sleep(1);
		   ui.CustomerSupportSendBtn.click();
		   ui.CustomerSupportAttachmentBtn.click();
		   ui.Sleep(10);
		   driver.navigate().back();
		   ui.Sleep(1);
		   driver.navigate().back();
		   ui.MENU.click();
		   log.info("CustomerSupport Test Successful");
		
	} catch (Exception e) {
		log.error("CustomerSupport Test Failed");
	}
	  
}
 public void CustomerSupportNumber() throws InterruptedException {
	   UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.CustomerSupportNumber.click();
		   ui.Sleep(7); 
		   driver.navigate().back();
		   //ui.MENU.click();
		   log.info("CustomerSupportNumber Test Successful");
		
	} catch (Exception e) {
		log.error("CustomerSupportNumber Test Failed");
	}
	   
 }
 public void HomeScreenTest() throws InterruptedException {
	  UserInterface ui = new UserInterface(driver);
	   ui.Sleep(2);
	   try {
		   ui.MENU.click();
		   ui.Sleep(2);
		   ui.MENU.click();
		   ui.FROM.click();
		   ui.Sleep(2);
		   ui.SearchListFromLoc.get(2).click();
		   ui.Sleep(3);
		   ui.TO.click();
		   ui.Sleep(2);
		   ui.SearchListToLoc.get(1).click();
		   ui.Sleep(3);
		   ui.WithInsurance.click();
		   ui.Pay.click();
		   ui.WalletHome.click();
		   ui.Sleep(1);
		   ui.Pay.click();
		   ui.Cash.click();
		   ui.WithInsurance.click();
		   ui.Sleep(2);		   
		   ui.ArrowLeft.click();
		   ui.ArrowRigth.click();
		   ui.ArrowRigth.click();
		   ui.ArrowRigth.click();
		   ui.ArrowLeft.click();
		   ui.ArrowLeft.click();
		   log.info("HomeScreen UI Test Successful");
		
	} catch (Exception e) {
		log.error("HomeScreen UI Test Failed");
	}
	   
}
}










