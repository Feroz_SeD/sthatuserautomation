package Utility;
import java.util.List;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;

public class UserInterface {
	public UserInterface(AndroidDriver<AndroidElement> driver) {
     	PageFactory.initElements(new AppiumFieldDecorator(driver), this);
	}
	public void Sleep(int X) throws InterruptedException {
		X = X*1000;
		Thread.sleep(X);
	}
	                                  //<<<<<<<<<Login>>>>>>>>>//
	
	//Main Screen before login
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvSthatUrl")
	public WebElement SthatUrl;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvDownloadSthatPartner")
	public WebElement DownloadSthatPartner;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvTermsAndConditions")
	public WebElement TermsAndConditions;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiBtnStart")
	public WebElement START;
	
	
	//Verify Mobile Number Screen
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiSpCountryCode")
	public WebElement SpCountryCode;
	
	  @AndroidFindBy(id="android:id/text1")     //+965(0),+966(1),+968(2),+971(3),+973(4),+974(5).
	  public List<WebElement> CountryCode;	
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtMobileNumber")
	public WebElement MobileNumber;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiBtnProceed")
	public WebElement SEND;

	
	//OTP screen
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvResendOtp")
	public WebElement resendcode;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtOtp")
	public WebElement EnterOtp;
	
	
	                                //<<<<<<<<<Signup>>>>>>>>>//
	
	//Create New Account Screen
	@AndroidFindBy(id="android:id/button1")
	public WebElement NewAccOK;
	
	@AndroidFindBy(id="android:id/button2")
	public WebElement NewAccCANCEL;
	
	
	//SignIn Mobile Number Screen
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtName")
	public WebElement EnterName;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtEmail")
	public WebElement EnterEmail;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiBtnProceed")
	public WebElement SIGNUP;
	
                                     //<<<<<<<<<HomeScreen>>>>>>>>>//
	
	//Menu
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvBurgerMenu")
	public WebElement MENU;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvLogout")
	  public WebElement Logout;
	  
	    @AndroidFindBy(id="android:id/button1")
	    public WebElement logoutYes;
	    
	    @AndroidFindBy(id="android:id/button2")
	    public WebElement logoutNo;
	  
	  @AndroidFindBy(className="android.widget.ImageButton")     //BackButton(0)
	  public List<WebElement> BackButton;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlBookingHistory")
	  public WebElement BookingHistory;
		  	  
	    @AndroidFindBy(xpath="//android.widget.TextView[@text='Completed']")
	    public WebElement BHCompleted;
	    
	    @AndroidFindBy(xpath="//android.widget.TextView[@text='Cancelled']")
	    public WebElement BHCancelled;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlWallet")
	  public WebElement WalletMenu;
	  
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvRefresh")
	    public WebElement Refresh;
	    
	    @AndroidFindBy(xpath="//android.widget.TextView[@text='www.tmmmt.com/banks']")
	    public WebElement BanksUrl;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtAmount")
	    public WebElement Amount;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvAddMoney")
	    public WebElement AddMoney;
	    
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/cardNumberET")//Credit Card
	      public WebElement CardNumber;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/expiryDateET")
	      public WebElement ExpiryDate;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/cvvET")
	      public WebElement Cvv;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/holderNameET")
	      public WebElement CardHolderName;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/rememberMeTB")
	      public WebElement RememberMe;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/payBtn")
	      public WebElement PayButton;
	      
	      @AndroidFindBy(id="android:id/button1")
	      public WebElement CancelPaymentYes;
	      
	      @AndroidFindBy(id="android:id/button2")
	      public WebElement CancelPaymentNo;
	      	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlSettings")
	  public WebElement Settings;
	  
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvEdit")
	    public WebElement SettingsEdit;
	    
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtUserName")
	      public WebElement ProfileName;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtEmail")
	      public WebElement ProfileEmail;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvUpdate")
	      public WebElement ProfileUpdate;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvCancel")
	      public WebElement ProfileCancel;	    
	    
	    @AndroidFindBy(xpath="//android.widget.TextView[@text='English']")
	    public WebElement LanguageEnglish;
	    
	    @AndroidFindBy(xpath="//android.widget.TextView[@text='العربية']")
	    public WebElement LanguageArabic;
	    
	      @AndroidFindBy(id="android:id/button1")
	      public WebElement languageApply;
	      
	      @AndroidFindBy(id="android:id/button2")
	      public WebElement languageDecline;	    
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiSwitchSmsNotification")
		public WebElement SmsNotification;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiSwitchEmailNotification")
		public WebElement EmailNotification;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvArrowRight")
		public WebElement Rating;	    
	    
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlShare")
	  public WebElement Share;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlChat")
	  public WebElement CustomerSupport;
	  
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/messageInput")
	    public WebElement CustomerSupportMsg;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/messageSendButton")
	    public WebElement CustomerSupportSendBtn;
	    
	    @AndroidFindBy(id="com.tmmmt.sthat.beta:id/attachmentButton")
	    public WebElement CustomerSupportAttachmentBtn;
	    
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlCamera")
	      public WebElement CustomerSupportCamera;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlFiles")
	      public WebElement CustomerSupportFiles;
	      
	      @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlVoice")
	      public WebElement CustomerSupportAudioMsg;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiLlSupport")
	  public WebElement CustomerSupportNumber;
	  
	  @AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvVersionNumber")
	  public WebElement Version;
	  
	  
	//HomeScreen
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtLoadingLocation")
	public WebElement FROM;
	
	  @AndroidFindBy(className="android.view.ViewGroup")     //Search with index 0,1,2...
	  public List<WebElement> SearchListFromLoc;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiEtUnloadingLocation")
	public WebElement TO;
	
	  @AndroidFindBy(className="android.view.ViewGroup")     //Search with index 0,1,2...
	  public List<WebElement> SearchListToLoc;
	
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiCbWithInsurance")
	public WebElement WithInsurance;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiTvPayType")
	public WebElement Pay;
	
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Cash']")
	  public WebElement Cash;
	  
	  @AndroidFindBy(xpath="//android.widget.TextView[@text='Wallet']")
	  public WebElement WalletHome;
	  
	@AndroidFindBy(className="android.widget.ImageView")     //Gps(0)
	public List<WebElement> Gps;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvArrowLeft")
	public WebElement ArrowLeft;
	  
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvArrowRight")
	public WebElement ArrowRigth;
	
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiCvPlaceOrder")
	public WebElement PlaceRequest;
	
	  @AndroidFindBy(id="android:id/button1")
	  public WebElement ChooseLocOK;
	  
	  @AndroidFindBy(id="android:id/button2")
	  public WebElement ChooseLocLATER;
	  
	@AndroidFindBy(id="com.tmmmt.sthat.beta:id/uiIvCancel")    //CanelRequest
	public WebElement CanelRequest;
	
      @AndroidFindBy(id="android:id/button1")
	  public WebElement CanelRequestYES;
	  
	  @AndroidFindBy(id="android:id/button2")
	  public WebElement CanelRequestNO;
	  

	  
	  
	
	
	
	                                 //<<<<<<<<<Order>>>>>>>>>//
	
}
