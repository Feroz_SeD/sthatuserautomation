package Utility;


import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.apache.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

public class PageHelper {
	Logger log = Logger.getLogger(PageHelper.class);
	
	public String getOTP(String mobileNo) throws Exception {
		String mobile = mobileNo;
		URL url = new URL("https://sthatnapibeta.tmmmt.com/api/v1/sms?mobileNumber="+mobile); 
		HttpURLConnection conn = (HttpURLConnection)url.openConnection(); 
		conn.setRequestMethod("GET"); 
		conn.connect(); 
		int responsecode = conn.getResponseCode();
		String inline = "";
		if(responsecode != 200)
			throw new RuntimeException("HttpResponseCode: " +responsecode);
		else
		{
		    Scanner sc = new Scanner(url.openStream());
			while(sc.hasNext())
			{
				inline+=sc.nextLine();
			}
			log.info("\n JSON data in string format ");
			log.info(inline);
			sc.close();
		}
		
		JSONParser parse = new JSONParser(); 
		JSONObject jobj = (JSONObject)parse.parse(inline); 
		JSONArray jsonarr_1 = (JSONArray) jobj.get("data"); 
		JSONObject jsonobj_1 = (JSONObject)jsonarr_1.get(0);
		String otp = (String) jsonobj_1.get("text");
		log.info("\n OTP: " + otp);
		return otp;
	}
	
}
