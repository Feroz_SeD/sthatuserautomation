package SUAutomation;


import java.io.IOException;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import SUAutomation.Main;

public class AppTest {
	@Test
	public  void test() throws Throwable {
		Main m = new Main();
		m.main();
	}
	@BeforeSuite
	public void startappium() throws IOException, InterruptedException {
		
		Runtime.getRuntime().exec("cmd /c start C:\\Appium\\appiumstart.bat");
		Thread.sleep(7000);
	}
	@AfterSuite
	public void stopServer() throws IOException {
		Runtime runtime = Runtime.getRuntime();{
		runtime.exec("taskkill /f /fi \"IMAGENAME eq node.exe\" /t");		
    }
	}
	
}
